from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

# Create your views here.

def Apoteker(request):
    template = loader.get_template('one.html')
    return HttpResponse(template.render())