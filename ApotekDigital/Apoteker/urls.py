from django.urls import path
from . import views

urlpatterns = [
    path('Apoteker/', views.Apoteker, name='Apoteker'),
]