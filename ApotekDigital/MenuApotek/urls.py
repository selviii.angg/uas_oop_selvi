from django.urls import path
from . import views

urlpatterns = [
    path('MenuApotek/', views.MenuApotek, name='MenuApotek'),
    path('MenuApoteks/', views.MenuApoteks, name='MenuApoteks'),
]