from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

def MenuApotek(request):
  Layout = loader.get_template('myfirst.html')
  return HttpResponse(Layout.render())

def MenuApoteks(request):
  templates = loader.get_template('mysecond.html')
  return HttpResponse(templates.render())