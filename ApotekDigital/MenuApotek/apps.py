from django.apps import AppConfig


class MenuapotekConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'MenuApotek'
